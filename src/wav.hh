#ifndef WAV_HH
# define WAV_HH

# include <sys/stat.h>
# include <fcntl.h>
# include <stdlib.h>
# include <sys/mman.h>
# include <unistd.h>

extern int global_fd;

struct wav
{
  const unsigned int    chunkID;
  const unsigned int    chunkSize;
  const unsigned int    format;
  const unsigned int    subchunk1ID;
  const unsigned int    subchunk1Size;
  const unsigned short  audioFormat;
  const unsigned short  numChannels;
  const unsigned int    sampleRate;
  const unsigned int    byteRate;
  const unsigned short  blockAlign;
  const unsigned short  bitsPerSample;
  const unsigned int    subchunk2ID;
  const unsigned int    subchunk2Size;
  char                  *data;
} __attribute__((__packed__));

struct wav *open_wav(const char *filename);

#endif /* WAV_HH */
