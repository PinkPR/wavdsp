#include "wav.hh"

int global_fd = 0;

struct wav *open_wav(const char *filename)
{
  struct wav  *wav_file = NULL;
  struct stat st;
  int         fd;

  if ((fd = open(filename, O_RDONLY)) == -1)
    return NULL;

  if ((stat(filename, &st)) == -1)
  {
    close(fd);
    return NULL;
  }

  wav_file = (struct wav *) mmap(NULL,
                                 st.st_size,
                                 PROT_READ,
                                 MAP_FILE,
                                 fd,
                                 0);

  if (wav_file == NULL)
  {
    close(fd);
    return NULL;
  }

  wav_file->data = ((char *) wav_file) + 40;
                   //(sizeof (struct wav) - sizeof (const unsigned char *)));
  global_fd = fd;

  return wav_file;
}
