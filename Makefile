CXX       = clang++
CXXFLAGS  = -std=c++11 -pedantic -Wall -Wextra -g3
TARGET    = wte

SRC       = $(addprefix src/,     \
              wav.cc              \
              main.cc             \
            )

all: $(TARGET)

$(TARGET): $(SRC)
	$(CXX) $(CXXFLAGS) -o $(TARGET) $(SRC)

clean:
	rm -f $(TARGET) *.o

.PHONY: clean
